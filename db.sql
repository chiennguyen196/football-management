CREATE TABLE teams (
  id INT PRIMARY KEY ,
  name VARCHAR(50) NOT NULL,
  captain_id BIGINT
);

CREATE TABLE leagues(
  id INT PRIMARY KEY ,
  name VARCHAR(100) NOT NULL
);

CREATE TABLE seasons (
  id INT PRIMARY KEY ,
  name VARCHAR(100) NOT NULL ,
  league_id INT REFERENCES leagues(id)
);

CREATE TABLE rounds(
  id BIGINT PRIMARY KEY ,
  index INT NOT NULL ,
  season_id INT REFERENCES seasons(id)
);


CREATE TABLE players (
  id BIGINT PRIMARY KEY ,
  name VARCHAR(50) NOT NULL ,
  age SMALLINT CHECK (age > 0 AND age < 100),
  height SMALLINT CHECK (height > 0 AND height < 200),
  weight SMALLINT CHECK (weight > 0),
  joined DATE,
  position VARCHAR(20),
  team_id INT REFERENCES teams(id)
);

CREATE TABLE matches (
  id BIGINT PRIMARY KEY ,
  home_team_id INT REFERENCES teams(id),
  away_team_id INT REFERENCES teams(id),
  start_time TIMESTAMP,
  round_id BIGINT REFERENCES rounds(id),
  home_team_score INT DEFAULT 0,
  away_team_score INT DEFAULT 0,

  CHECK (home_team_id != away_team_id)
);

CREATE TABLE player_match (
  id BIGINT PRIMARY KEY ,
  match_id BIGINT NOT NULL REFERENCES matches(id),
  player_id BIGINT NOT NULL REFERENCES players(id),
  yellow_card SMALLINT,
  red_card SMALLINT,
  goal_number SMALLINT,
  own_goal_number SMALLINT,
  UNIQUE (match_id, player_id)
);


CREATE TABLE season_team (
  season_id INT REFERENCES seasons(id),
  team_id INT REFERENCES teams(id),
  PRIMARY KEY (season_id, team_id)
);

ALTER TABLE teams ADD FOREIGN KEY (captain_id) REFERENCES players(id);


-- ALTER TABLE player_match ADD UNIQUE (match_id, player_id);
--
-- ALTER TABLE matches ADD FOREIGN KEY (season_id) REFERENCES seasons(id);


DROP SEQUENCE IF EXISTS hibernate_sequence;
CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.hibernate_sequence OWNER TO postgres;


