<%--
  Created by IntelliJ IDEA.
  User: Chien Nguyen
  Date: 30/06/2017
  Time: 10:52 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">My Football Management</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Trận đấu
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/match">Tất cả</a></li>
                    <li><a href="/match/upcoming">Sắp diễn ra</a></li>
                    <li><a href="/match/has-taken-place">Đã điễn ra</a></li>
                </ul>
            </li>


            <li><a href="/team">Team</a></li>
            <li><a href="/player">Player</a></li>
        </ul>
    </div>
</nav>
