<%--
  Created by IntelliJ IDEA.
  User: Chien Nguyen
  Date: 30/06/2017
  Time: 10:52 AM
  To change this template use File | Settings | File Templates.
--%>
<!-- Latest compiled and minified CSS -->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
    .container {
        margin-top: auto;
        padding: 50px;
    }
    table {
        padding: 2px;
        margin-top: 5px;
    }
    .panel-heading {
        font-weight: bold;
    }
</style>
