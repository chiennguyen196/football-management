<%--
  Created by IntelliJ IDEA.
  User: Chien Nguyen
  Date: 07/07/2017
  Time: 8:58 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <%@include file="./includes/head.jsp" %>
    <title>${title}</title>
</head>
<body>
<%@include file="./includes/header.jsp" %>
<div class="container">
    <header>
        <h2>${title}</h2>
    </header>
    <div class="row">
        <div class="col-md-8">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Sắp diễn ra</div>
                    <div class="panel-body">
                        <c:forEach items="${upcomingMatches}" var="match">
                            <div class="entry row">
                                <div class="col-md-4 text-left">
                                    <b>${match.homeTeam.name}</b>
                                </div>
                                <div class="col-md-4 text-center">
                                    <p><fmt:formatDate value="${match.startTime}" pattern="dd/MM/yyyy"/></p>
                                    <h4><fmt:formatDate value="${match.startTime}" pattern="HH:mm"/></h4>
                                </div>
                                <div class="col-md-4 text-right">
                                    <b>${match.awayTeam.name}</b>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Vừa diễn ra</div>
                    <div class="panel-body">
                        <c:forEach items="${takenPlaceMatches}" var="match">
                            <div class="entry row">
                                <div class="col-md-4 text-left">
                                    <b>${match.homeTeam.name}</b>
                                </div>
                                <div class="col-md-4 text-center">
                                    <p><fmt:formatDate value="${match.startTime}" pattern="dd/MM/yyyy"/></p>
                                    <h4>${match.homeTeamScore} - ${match.awayTeamScore}</h4>
                                </div>
                                <div class="col-md-4 text-right">
                                    <b>${match.awayTeam.name}</b>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Bảng xếp hạng</div>
                    <div class="panel-body" style="padding: 0;">
                        <table class="table table-striped">
                            <thead>
                            <th>No.</th>
                            <th>Team</th>
                            <th>Played</th>
                            <th>Won</th>
                            <th>Drawn</th>
                            <th>Lost</th>
                            <th>For</th>
                            <th>Against</th>
                            </thead>
                            <tbody>
                            <c:forEach items="${teamAchievenmentList}" varStatus="i" var="team">
                                <tr>
                                    <td>${i.index + 1}</td>
                                    <td>${team.teamName}</td>
                                    <td>${team.played}</td>
                                    <td>${team.won}</td>
                                    <td>${team.drawn}</td>
                                    <td>${team.lost}</td>
                                    <td>${team.forNum}</td>
                                    <td>${team.against}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">Vua phá lưới</div>
                <div class="panel-body" style="padding: 0;">
                    <table class="table table-striped">
                        <thead>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Team</th>
                        <th>Goals</th>
                        </thead>
                        <tbody>
                        <c:forEach items="${listPlayerAchievenment}" varStatus="i" var="playerAchievementDTO">
                            <tr>
                                <td>${i.index + 1}</td>
                                <td>${playerAchievementDTO.name}</td>
                                <td>${playerAchievementDTO.position}</td>
                                <td>${playerAchievementDTO.teamName}</td>
                                <td>${playerAchievementDTO.goalNumber}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="./includes/footer.jsp" %>
</body>
</html>
