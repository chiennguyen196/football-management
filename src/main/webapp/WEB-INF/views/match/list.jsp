<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="current" class="java.util.Date" />
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <%@include file="../includes/head.jsp"%>
    <title>${title}</title>
</head>
<body>
<%@include file="../includes/header.jsp"%>
<div class="container">
    <c:choose>
        <c:when test="${message}">
            <div class="row alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <span>${message}</span>
            </div>
        </c:when>
    </c:choose>

    <header>
        <h2>${title}</h2>
    </header>
    <div class="row">
        <a href="/match/add-new" class="btn btn-success pull-right" role="button">
            <span class="glyphicon glyphicon-plus"></span> Thêm mới
        </a>
    </div>
    <div class="content">
        <table class="table table-bordered table-hover">
            <thead>
            <th>No</th>
            <th>Round</th>
            <th>Home Team</th>
            <th>Start Time</th>
            <th>Away Team</th>
            <th>Action</th>
            </thead>
            <tbody>
            <c:forEach items="${matches}" var="match">
                <tr>
                    <td>${match.id}</td>
                    <td>${match.round.name}</td>
                    <td>${match.homeTeam.name}</td>
                    <td class="text-center">
                        <fmt:formatDate value="${match.startTime}" pattern="dd/MM/yyyy HH:mm:ss" />
                        <c:choose>
                            <c:when test="${current > match.startTime}">
                                <br>
                                ${match.homeTeamScore} - ${match.awayTeamScore}
                            </c:when>
                            <c:otherwise>

                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>${match.awayTeam.name}</td>

                    <td>
                        <a href="<c:url value='/match/${match.id}/edit'/>" >
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<c:url value='/match/${match.id}/delete'/>" >
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>

                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<%@include file="../includes/footer.jsp"%>
</body>
</html>
