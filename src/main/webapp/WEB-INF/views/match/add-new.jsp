<%--
  Created by IntelliJ IDEA.
  User: Chien Nguyen
  Date: 30/06/2017
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <%@include file="../includes/head.jsp"%>
    <title>${title}</title>
</head>
<body>
<%@include file="../includes/header.jsp"%>
<div class="container">
    <header>
        <h2>${title} ${player.position}</h2>
    </header>
    <div class="content">
        <div class="row">
            <form:form method="post" action="/match/add-new" modelAttribute="matchDTO" name="myForm">
                <form:input path="id" id="id" type="hidden"/>
                <div class="form-group">
                    <label>Home Team:</label>
                    <form:select path="homeTeam.id" cssClass="form-control" id="homeTeam">
                        <form:options items="${teams}"/>
                    </form:select>
                    <em><form:errors path="homeTeam" cssClass="error"/></em>
                </div>
                <div class="form-group">
                    <label>Away Team:</label>
                    <form:select path="awayTeam.id" cssClass="form-control" id="awayTeam">
                        <form:options items="${teams}"/>
                    </form:select>
                    <em><form:errors path="awayTeam" cssClass="error"/></em>
                </div>
                <div class="form-group">
                    <label>Start Time:</label>
                    <form:input path="startTime" type="datetime-local" cssClass="form-control" id="startTime"/>
                    <em><form:errors path="startTime" cssClass="error"/></em>
                </div>
                <div class="form-group">
                    <label>Round:</label>
                    <form:select path="round.id" cssClass="form-control">
                        <form:options items="${rounds}"/>
                    </form:select>
                    <em><form:errors path="round" cssClass="error"/></em>
                </div>

            </form:form>
            <button type="button" onclick="test()" class="btn btn-primary">Save</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {

    });
    function test() {
        console.log("fuck u");
        if($("#homeTeam").val() == $("#awayTeam").val()){
            alert("Hai doi khong duoc trung nhau")
            return false;
        }



        if(!$("#startTime").val()){
            alert("thoi gian bat dau khong duoc de trong!")
            return false;
        }

        $("form").submit();
    }
</script>
<%@include file="../includes/footer.jsp"%>
</body>
</html>