<%--
  Created by IntelliJ IDEA.
  User: Chien Nguyen
  Date: 30/06/2017
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <%@include file="../includes/head.jsp" %>
    <title>${title}</title>
</head>
<body>
<%@include file="../includes/header.jsp" %>
<div class="container">
    <header>
        <h2>${title}</h2>
    </header>
    <div class="content">
        <form:form modelAttribute="playerMatchDTOWrapper" method="post" action="/match/edit" cssClass="form-inline">
            <form:input path="match.homeTeam.id" type="hidden"/>
            <form:input path="match.awayTeam.id" type="hidden"/>
            <form:input path="match.startTime" type="hidden"/>
            <form:input path="match.round.id" type="hidden"/>
        <div class="row">

                <form:input path="match.id" type="hidden"/>
                <div class="col-md-6 text-center">
                    <div class="form-group">
                        <label>${match.homeTeam.name}

                        <form:input
                                path="match.homeTeamScore"
                                cssClass="form-control"
                                type="number" min="0" step="1" max="100"/> score</label>
                    </div>
                    <div class="list-player">
                        <table class="table-bordered table table-hover">
                            <thead>
                            <th>Tên</th>
                            <th>Thẻ vàng</th>
                            <th>Thẻ đỏ</th>
                            <th>Bàn thắng</th>
                            <th>Phản lưới</th>
                            </thead>
                            <tbody>
                            <c:forEach items="${playerMatchHomeTeamList}" varStatus="stt">
                                <form:input
                                        path="playerMatchHomeTeamList[${stt.index}].id"
                                        type="hidden"/>
                                <form:input
                                        path="playerMatchHomeTeamList[${stt.index}].player.id"
                                        type="hidden"/>
                                <form:input
                                        path="playerMatchHomeTeamList[${stt.index}].match.id"
                                        type="hidden"/>
                                <tr>
                                    <td>${playerMatchHomeTeamList[stt.index].player.name}</td>
                                    <td>
                                        <form:input
                                                path="playerMatchHomeTeamList[${stt.index}].yellowCard"
                                                type="number" min="0" step="1" max="2" cssClass="form-control"/>
                                    </td>
                                    <td>
                                        <form:input
                                                path="playerMatchHomeTeamList[${stt.index}].redCard"
                                                type="number" min="0" step="1" max="1" cssClass="form-control"/>
                                    </td>
                                    <td>
                                        <form:input
                                                path="playerMatchHomeTeamList[${stt.index}].goalNumber"
                                                type="number" min="0" step="1" max="90" cssClass="form-control"/>
                                    </td>
                                    <td>
                                        <form:input
                                                path="playerMatchHomeTeamList[${stt.index}].ownGoalNumber"
                                                type="number" min="0" step="1" max="90" cssClass="form-control"/>
                                    </td>
                                </tr>

                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div class="form-group">
                        <label>${match.awayTeam.name}
                        <form:input
                                path="match.awayTeamScore"
                                cssClass="form-control"
                                type="number" min="0" step="1" max="100"/> score</label>
                    </div>
                    <div class="list-player">
                        <table class="table-bordered table table-hover">
                            <thead>
                            <th>Tên</th>
                            <th>Thẻ vàng</th>
                            <th>Thẻ đỏ</th>
                            <th>Bàn thắng</th>
                            <th>Phản lưới</th>
                            </thead>
                            <tbody>
                            <c:forEach items="${playerMatchAwayTeamList}" varStatus="stt">
                                <form:input
                                        path="playerMatchAwayTeamList[${stt.index}].id"
                                        type="hidden"/>
                                <form:input
                                        path="playerMatchAwayTeamList[${stt.index}].player.id"
                                        type="hidden"/>
                                <form:input
                                        path="playerMatchAwayTeamList[${stt.index}].match.id"
                                        type="hidden"/>
                                <tr>
                                    <td>${playerMatchAwayTeamList[stt.index].player.name}</td>
                                    <td>
                                        <form:input
                                                path="playerMatchAwayTeamList[${stt.index}].yellowCard"
                                                type="number" min="0" step="1" max="2"
                                                cssClass="form-control"
                                                />
                                    </td>
                                    <td>
                                        <form:input
                                                path="playerMatchAwayTeamList[${stt.index}].redCard"
                                                type="number" min="0" step="1" max="1" cssClass="form-control"/>
                                    </td>
                                    <td>
                                        <form:input
                                                path="playerMatchAwayTeamList[${stt.index}].goalNumber"
                                                type="number" min="0" step="1" max="90" cssClass="form-control"/>
                                    </td>
                                    <td>
                                        <form:input
                                                path="playerMatchAwayTeamList[${stt.index}].ownGoalNumber"
                                                type="number" min="0" step="1" max="90" cssClass="form-control"/>
                                    </td>
                                </tr>

                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>


        </div>
        <div class="row text-center">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        </form:form>
    </div>
</div>

<%@include file="../includes/footer.jsp" %>
</body>
</html>