<%--
  Created by IntelliJ IDEA.
  User: Chien Nguyen
  Date: 30/06/2017
  Time: 10:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <%@include file="../includes/head.jsp"%>
    <title>${title}</title>
</head>
<body>
<%@include file="../includes/header.jsp"%>
<div class="container">
    <c:choose>
        <c:when test="${message}">
            <div class="row alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <span>${message}</span>
            </div>
        </c:when>
    </c:choose>

    <header>
        <h2>${title}</h2>
    </header>
    <div class="row">
        <a href="/player/create" class="btn btn-success pull-right" role="button">
            <span class="glyphicon glyphicon-plus"></span> Thêm mới
        </a>
    </div>
    <div class="content">
        <table class="table table-bordered table-hover">
            <thead>
            <th>No</th>
            <th>Image</th>
            <th>Name</th>
            <th>Age</th>
            <th>Position</th>
            <th>Edit</th>
            <th>Delete</th>
            </thead>
            <tbody>
            <c:forEach items="${players}" var="player">
                <tr>
                    <td>${player.id}</td>
                    <td>
                        <img src="https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg"
                             height="150px" width="150px">
                    </td>
                    <td>${player.name}</td>
                    <td>${player.age}</td>
                    <td>${player.position}</td>
                    <td>
                        <a href="<c:url value='/player/edit/${player.id}'/>" >
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    <td>
                        <a href="<c:url value='/player/delete/${player.id}'/>" >
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<%@include file="../includes/footer.jsp"%>
</body>
</html>
