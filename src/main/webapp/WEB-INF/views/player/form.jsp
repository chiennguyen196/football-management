<%--
  Created by IntelliJ IDEA.
  User: Chien Nguyen
  Date: 30/06/2017
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <%@include file="../includes/head.jsp"%>
    <title>${title}</title>
</head>
<body>
<%@include file="../includes/header.jsp"%>
<div class="container">
    <header>
        <h2>${title} ${player.position}</h2>
    </header>
    <div class="content">
        <div class="row">
            <form:form method="post" action="/player/save" modelAttribute="playerDTO" >
                <form:input path="id" id="id" type="hidden"/>
                <form:input path="team.id" type="hidden"/>
                <form:input path="team.name" type="hidden"/>
                <div class="form-group">
                    <label>Name:</label>
                    <form:input path="name" type="text" cssClass="form-control"/>
                    <em><form:errors path="name" cssClass="error"/></em>
                </div>
                <div class="form-group">
                    <label>Age:</label>
                    <form:input path="age" type="number" cssClass="form-control" max="100" min="1" step="1"/>
                    <em><form:errors path="age" cssClass="error"/></em>
                </div>
                <div class="form-group">
                    <label>Height:</label>
                    <form:input path="height" type="number" cssClass="form-control" max="300" min="1" step="1"/>
                    <em><form:errors path="height" cssClass="error"/></em>
                </div>
                <div class="form-group">
                    <label>Weight:</label>
                    <form:input path="weight" type="number" cssClass="form-control" max="200" min="1" step="1"/>
                    <em><form:errors path="weight" cssClass="error"/></em>
                </div>
                <div class="form-group">
                    <label>Joined Date:</label>
                    <form:input path="joined" type="date" cssClass="form-control"/>
                    <em><form:errors path="joined" cssClass="error"/></em>
                </div>
                <div class="form-group">
                    <label>Position:</label>
                    <label class="radio-inline">
                        <input type="radio" path="position" name="position" value="Goalkeeper">Goalkeeper
                    </label>
                    <label class="radio-inline">
                        <input type="radio" path="position" name="position" value="Defender">Defender
                    </label>
                    <label class="radio-inline">
                        <input type="radio" path="position" name="position" value="Midfielder">Midfielder
                    </label>
                    <label class="radio-inline">
                        <input type="radio" path="position" name="position" value="Attacking">Attacking
                    </label>
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form:form>
        </div>
    </div>
</div>
<%@include file="../includes/footer.jsp"%>
</body>
</html>