<%--
  Created by IntelliJ IDEA.
  User: Chien Nguyen
  Date: 30/06/2017
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <%@include file="../includes/head.jsp"%>
    <title>${title}</title>
</head>
<body>
<%@include file="../includes/header.jsp"%>
<div class="container">
    <header>
        <h2>${title} ${player.position}</h2>
    </header>
    <div class="content">
        <div class="row">
            <form:form method="post" action="/team/save" modelAttribute="teamDTO" >
                <form:input path="id" id="id" type="hidden"/>
                <div class="form-group">
                    <label>Name:</label>
                    <form:input path="name" type="text" cssClass="form-control"/>
                    <em><form:errors path="name" cssClass="error"/></em>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form:form>
        </div>
    </div>
</div>
<%@include file="../includes/footer.jsp"%>
</body>
</html>