package com.footballmanagement.dao;

import com.footballmanagement.model.BaseEntity;

import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
public interface BaseDAO<T extends BaseEntity> {

    T findById(Long id);

    List<T> findAll();

    void deleteById(Long id);

    void delete(T entity);

    void insert(T entity);

    void saveOrUpdate(T entity);

    void update(T entity);

    T merge(T entity);

    Long count();

}
