package com.footballmanagement.dao;

import com.footballmanagement.dto.PlayerAchievementDTO;
import com.footballmanagement.dto.TeamAchievementDTO;
import com.footballmanagement.model.Match;

import java.util.List;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
public interface MatchDAO extends BaseDAO<Match>{
    List<Match> findUpcomingMatches();

    List<Match> findMatchesHasTakenPlace();

    void removeMatchesByTeamId(Long id);

    List<TeamAchievementDTO> getTeamAchievenmentList(Integer limit);
}
