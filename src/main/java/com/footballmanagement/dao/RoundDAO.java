package com.footballmanagement.dao;

import com.footballmanagement.model.Round;

/**
 * Created by Chien Nguyen on 05/07/2017.
 */
public interface RoundDAO extends BaseDAO<Round> {

}
