package com.footballmanagement.dao;

import com.footballmanagement.dto.PlayerAchievementDTO;
import com.footballmanagement.model.PlayerMatch;

import java.util.List;

/**
 * Created by Chien Nguyen on 06/07/2017.
 */
public interface PlayerMatchDAO extends BaseDAO<PlayerMatch> {
    void deleteByMatchId(Long matchId);

    void deleteByPlayerId(Long playerId);

    PlayerMatch getByPlayerIdAndMatchId(Long playerId, Long matchId);

    List<PlayerAchievementDTO> getListPlayerAchievenment(Integer limit);
}
