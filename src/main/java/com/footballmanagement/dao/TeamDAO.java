package com.footballmanagement.dao;

import com.footballmanagement.model.Player;
import com.footballmanagement.model.Team;

import java.util.List;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
public interface TeamDAO extends BaseDAO<Team>{
    List<Team> search(String q);

    void removePlayer(Long teamId, Long playerId);

    void addPlayer(Long team, Long playerId);

    void removeAllPlayers(Long teamId);

    List<Player> getAllPlayer(Long teamId);
}
