package com.footballmanagement.dao;

import com.footballmanagement.model.Player;
import com.footballmanagement.model.Team;

import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
public interface PlayerDAO extends BaseDAO<Player> {
    List<Player> searchByName(String name);

    List<Player> listAllPlayersNotInAnyTeam();

    void addPlayerToTeam(Team teamId, Long playerId);

    void removePlayerFromTheTeam(Long playerId);

    void setTeamIsNull(Long teamId);

    List<Player> getPlayersByTeamId(Long teamId);
}
