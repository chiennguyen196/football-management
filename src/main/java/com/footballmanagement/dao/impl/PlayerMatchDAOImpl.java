package com.footballmanagement.dao.impl;

import com.footballmanagement.dao.PlayerMatchDAO;
import com.footballmanagement.dto.PlayerAchievementDTO;
import com.footballmanagement.model.PlayerMatch;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by Chien Nguyen on 06/07/2017.
 */
@Repository
public class PlayerMatchDAOImpl extends AbstractDAO<PlayerMatch> implements PlayerMatchDAO {
    @Override
    public void deleteByMatchId(Long matchId) {
        Query query = getSession().createQuery("delete from " + PlayerMatch.class.getName()
                + " where match.id = :matchId");
        query.setParameter("matchId", matchId);
        query.executeUpdate();
    }

    @Override
    public void deleteByPlayerId(Long playerId) {
        Query query = getSession().createQuery("delete from " + PlayerMatch.class.getName()
                + " where player.id = :playerId");
        query.setParameter("playerId", playerId);
        query.executeUpdate();
    }

    @Override
    public PlayerMatch getByPlayerIdAndMatchId(Long playerId, Long matchId) {
        Query query = getSession().createQuery("from " + PlayerMatch.class.getName()
                + " where player.id = :playerId and match.id = :matchId");
        query.setParameter("playerId", playerId);
        query.setParameter("matchId", matchId);
        return (PlayerMatch) query.uniqueResult();
    }

    @Override
    public List<PlayerAchievementDTO> getListPlayerAchievenment(Integer limit) {
        Query query = getSession().createQuery(" select new " + PlayerAchievementDTO.class.getName()
                + "(player.id, player.name, player.position, player.team.name, sum (goalNumber))"
                + " from " + PlayerMatch.class.getName()
                + " group by player.id, player.name, player.position, player.team.name"
                + " order by sum(goalNumber) desc ");
        query.setMaxResults(limit);
        return (List<PlayerAchievementDTO>) query.list();
    }
}
