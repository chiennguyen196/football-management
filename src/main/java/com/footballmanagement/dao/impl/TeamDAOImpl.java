package com.footballmanagement.dao.impl;

import com.footballmanagement.dao.PlayerDAO;
import com.footballmanagement.dao.TeamDAO;
import com.footballmanagement.model.Player;
import com.footballmanagement.model.Team;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
@Repository("teamDAO")
public class TeamDAOImpl extends AbstractDAO<Team> implements TeamDAO {

    @Autowired
    private PlayerDAO playerDAO;

    @Override
    public List<Team> search(String q) {
        Query query = getSession().createQuery("from " + Team.class.getName() +
            " where name = :name");
        query.setParameter("name", "%"+q+"%");
        return (List<Team>) query.list();
    }

    @Override
    public void removePlayer(Long teamId, Long playerId) {
        Query query = getSession().createQuery("update from " + Player.class.getName() +
            " set team = null where id = :playerId and team.id = :teamId");
        query.setParameter("playerId", playerId);
        query.setParameter("teamId", teamId);
        query.executeUpdate();
    }

    @Override
    public void addPlayer(Long teamId, Long playerId) {
        System.out.println("Chạy vào đây rồi!");
        Team team = findById(teamId);
        Player player = playerDAO.findById(playerId);
        team.getPlayerList().add(player);
        getSession().save(team);
    }

    @Override
    public void removeAllPlayers(Long teamId) {
        Query query = getSession().createQuery("update from " + Player.class.getName() +
                " set team = null where team.id = :teamId");
        query.setParameter("teamId", teamId);
        query.executeUpdate();
    }

    @Override
    public List<Player> getAllPlayer(Long teamId) {
        return findById(teamId).getPlayerList();
    }
}
