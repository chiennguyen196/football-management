package com.footballmanagement.dao.impl;

import com.footballmanagement.dao.MatchDAO;
import com.footballmanagement.dto.TeamAchievementDTO;
import com.footballmanagement.model.Match;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;


import java.util.LinkedList;
import java.util.List;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
@Repository
public class MatchDAOimpl extends AbstractDAO<Match> implements MatchDAO {
    @Override
    public List<Match> findUpcomingMatches() {
        Query query = getSession().createQuery("from " + Match.class.getName()
                + " where startTime > current_timestamp order by startTime asc ");

        return (List<Match>) query.list();
    }

    @Override
    public List<Match> findMatchesHasTakenPlace() {
        Query query = getSession().createQuery("from " + Match.class.getName()
                + " where startTime < current_timestamp order by startTime desc ");

        return (List<Match>) query.list();
    }

    @Override
    public void removeMatchesByTeamId(Long id) {
        Query query = getSession().createQuery("delete from " + Match.class.getName()
                + " where homeTeam.id = :id or awayTeam.id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<TeamAchievementDTO> getTeamAchievenmentList(Integer limit) {
        SQLQuery query = getSession().createSQLQuery(
                "SELECT team_id, teams.name, sum(played) as played,\n" +
                        "  sum(won) AS won, sum(drawn) AS drawn, sum(lose) AS lose,\n" +
                        "  sum(fornum) AS fornum, sum(against) AS against\n" +
                        "FROM (\n" +
                        "  SELECT home_team_id as team_id, count(id) AS played,\n" +
                        "  sum(CASE WHEN home_team_score > away_team_score THEN 1 ELSE 0 END) AS won,\n" +
                        "  sum(CASE WHEN home_team_score = away_team_score THEN 1 ELSE 0 END) AS drawn,\n" +
                        "  sum(CASE WHEN home_team_score < away_team_score THEN 1 ELSE 0 END) AS lose,\n" +
                        "  sum(home_team_score) AS fornum,\n" +
                        "  sum(away_team_score) AS against\n" +
                        "FROM matches\n" +
                        "GROUP BY home_team_id\n" +
                        "UNION ALL\n" +
                        "SELECT away_team_id as team_id, count(id) AS played,\n" +
                        "  sum(CASE WHEN home_team_score < away_team_score THEN 1 ELSE 0 END) AS won,\n" +
                        "  sum(CASE WHEN home_team_score = away_team_score THEN 1 ELSE 0 END) AS drawn,\n" +
                        "  sum(CASE WHEN home_team_score > away_team_score THEN 1 ELSE 0 END) AS lose,\n" +
                        "  sum(away_team_score) AS fornum,\n" +
                        "  sum(home_team_score) AS against\n" +
                        "FROM matches\n" +
                        "GROUP BY away_team_id\n" +
                        ") AS sub\n" +
                        "  LEFT JOIN teams ON sub.team_id = id\n" +
                        "GROUP BY team_id, teams.name\n" +
                        "ORDER BY won DESC , lose ASC , fornum DESC"
        );

        query.setMaxResults(limit);

        List<TeamAchievementDTO> list = new LinkedList<>();

        List<Object[]> rows = query.list();
        for(Object[] row : rows){
            TeamAchievementDTO temp = new TeamAchievementDTO(
                    Long.parseLong(row[0].toString()),
                    row[1].toString(),
                    Integer.parseInt(row[2].toString()),
                    Integer.parseInt(row[3].toString()),
                    Integer.parseInt(row[4].toString()),
                    Integer.parseInt(row[5].toString()),
                    Integer.parseInt(row[6].toString()),
                    Integer.parseInt(row[7].toString())
            );
            list.add(temp);
        }
        return list;
    }

}
