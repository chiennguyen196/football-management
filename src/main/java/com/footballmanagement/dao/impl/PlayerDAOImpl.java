package com.footballmanagement.dao.impl;

import com.footballmanagement.dao.PlayerDAO;
import com.footballmanagement.model.Player;
import com.footballmanagement.model.Team;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
@Repository("playerDAO")
public class PlayerDAOImpl extends AbstractDAO<Player> implements PlayerDAO {
    @Override
    public List<Player> searchByName(String name) {
        Query query = getSession().createQuery("from " + Player.class.getName() + " where lower(name) = :name");
        query.setParameter("name", "%" + name.toLowerCase() + "%");
        return (List<Player>) query.list();
    }

    @Override
    public List<Player> listAllPlayersNotInAnyTeam() {
        Query query = getSession().createQuery("from " + Player.class.getName()
                + " where team = null");
        return (List<Player>) query.list();
    }

    @Override
    public void addPlayerToTeam(Team team, Long playerId) {
        Player player = findById(playerId);
        player.setTeam(team);
   }

    @Override
    public void removePlayerFromTheTeam(Long playerId) {
        Player player = findById(playerId);
        player.setTeam(null);
    }

    @Override
    public void setTeamIsNull(Long teamId) {
        Query query = getSession().createQuery("update from "+Player.class.getName()
                + " set team = null where team.id = :teamId");
        query.setParameter("teamId", teamId);
        query.executeUpdate();
    }

    @Override
    public List<Player> getPlayersByTeamId(Long teamId) {
        Query query = getSession().createQuery("from " + Player.class.getName()
                + " where team.id = " + teamId.toString());
        return (List<Player>) query.list();
    }
}
