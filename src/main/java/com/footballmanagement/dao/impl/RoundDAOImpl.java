package com.footballmanagement.dao.impl;

import com.footballmanagement.dao.RoundDAO;
import com.footballmanagement.model.Round;
import org.springframework.stereotype.Repository;

/**
 * Created by Chien Nguyen on 05/07/2017.
 */
@Repository
public class RoundDAOImpl extends AbstractDAO<Round> implements RoundDAO {
}
