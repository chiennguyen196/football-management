package com.footballmanagement.dao.impl;

import com.footballmanagement.model.BaseEntity;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
public abstract class AbstractDAO<T extends BaseEntity> {
    private final Class<T> persistentClass;

    public AbstractDAO() {
        this.persistentClass =
                (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public T findById(Long key){
        return (T) getSession().get(persistentClass, key);
    }

    public void persist(T entity){
        getSession().persist(entity);
    }

    public void delete(T entity){
        getSession().delete(entity);
    }

    public void update(T entity){
        getSession().update(entity);
    }

    public void saveOrUpdate(T entity){
        getSession().saveOrUpdate(entity);
    }

    public void insert(T entity){
        getSession().save(entity);
    }

    public void deleteById(Long id){
        Query query = getSession().createQuery("delete from "+ persistentClass.getName()+ " where id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    public T merge(T entity){
        return (T) getSession().merge(entity);
    }

    public Long count(){
        return (Long) createEntityCriteria().setProjection(Projections.rowCount()).uniqueResult();
    }

    public List<T> findAll(){
        return createEntityCriteria().list();
    }

    protected Criteria createEntityCriteria(){
        return getSession().createCriteria(persistentClass);
    }
}
