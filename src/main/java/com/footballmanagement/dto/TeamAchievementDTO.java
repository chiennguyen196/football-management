package com.footballmanagement.dto;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by Chien Nguyen on 07/07/2017.
 */
@Entity
public class TeamAchievementDTO extends BaseDTO {

    @Column(name = "name")
    private String teamName;

    @Column(name = "played")
    private Integer played;

    @Column(name = "won")
    private Integer won;

    @Column(name = "drawn")
    private Integer drawn;

    @Column(name = "lost")
    private Integer lost;

    @Column(name = "fornum")
    private Integer forNum;

    @Column(name = "against")
    private Integer against;

    public TeamAchievementDTO() {
    }

    public TeamAchievementDTO(Long id, String teamName,
                              Integer played, Integer won, Integer drawn,
                              Integer lost, Integer forNum, Integer against) {
        this.setId(id);
        this.teamName = teamName;
        this.played = played;
        this.won = won;
        this.drawn = drawn;
        this.lost = lost;
        this.forNum = forNum;
        this.against = against;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Integer getPlayed() {
        return played;
    }

    public void setPlayed(Integer played) {
        this.played = played;
    }

    public Integer getWon() {
        return won;
    }

    public void setWon(Integer won) {
        this.won = won;
    }

    public Integer getDrawn() {
        return drawn;
    }

    public void setDrawn(Integer drawn) {
        this.drawn = drawn;
    }

    public Integer getLost() {
        return lost;
    }

    public void setLost(Integer lost) {
        this.lost = lost;
    }

    public Integer getForNum() {
        return forNum;
    }

    public void setForNum(Integer forNum) {
        this.forNum = forNum;
    }

    public Integer getAgainst() {
        return against;
    }

    public void setAgainst(Integer against) {
        this.against = against;
    }
}
