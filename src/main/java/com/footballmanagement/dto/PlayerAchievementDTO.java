package com.footballmanagement.dto;

/**
 * Created by Chien Nguyen on 07/07/2017.
 */
public class PlayerAchievementDTO extends BaseDTO {
    private String name;
    private String position;
    private String teamName;
    private Long goalNumber;

    public PlayerAchievementDTO() {
    }

    public PlayerAchievementDTO(Long id, String name, String position, String teamName, Long goalNumber) {
        this.setId(id);
        this.name = name;
        this.position = position;
        this.teamName = teamName;
        this.goalNumber = goalNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Long getGoalNumber() {
        return goalNumber;
    }

    public void setGoalNumber(Long goalNumber) {
        this.goalNumber = goalNumber;
    }
}
