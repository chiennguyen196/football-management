package com.footballmanagement.dto;

import com.footballmanagement.model.Player;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Chien Nguyen on 03/07/2017.
 */

public class PlayerDTO extends BaseDTO {

    private String name;

    private Integer age;

    private Integer height;

    private Integer weight;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date joined;

    private String position;

    private TeamDTO team;

    public PlayerDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Date getJoined() {
        return joined;
    }

    public void setJoined(Date joined) {
        this.joined = joined;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getId(){
        return super.getId();
    }

    public void setId(Long id){
        super.setId(id);
    }

    public TeamDTO getTeam() {
        return team;
    }

    public void setTeam(TeamDTO team) {
        this.team = team;
    }
}
