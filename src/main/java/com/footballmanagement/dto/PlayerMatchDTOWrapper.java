package com.footballmanagement.dto;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Chien Nguyen on 06/07/2017.
 */
public class PlayerMatchDTOWrapper {
    private List<PlayerMatchDTO> playerMatchHomeTeamList;
    private List<PlayerMatchDTO> playerMatchAwayTeamList;

    private MatchDTO match;

    public PlayerMatchDTOWrapper() {
        playerMatchHomeTeamList = new LinkedList<>();
        playerMatchAwayTeamList = new LinkedList<>();
    }

    public MatchDTO getMatch() {
        return match;
    }

    public void setMatch(MatchDTO match) {
        this.match = match;
    }

    public List<PlayerMatchDTO> getPlayerMatchHomeTeamList() {
        return playerMatchHomeTeamList;
    }

    public void setPlayerMatchHomeTeamList(List<PlayerMatchDTO> playerMatchHomeTeamList) {
        this.playerMatchHomeTeamList = playerMatchHomeTeamList;
    }

    public List<PlayerMatchDTO> getPlayerMatchAwayTeamList() {
        return playerMatchAwayTeamList;
    }

    public void setPlayerMatchAwayTeamList(List<PlayerMatchDTO> playerMatchAwayTeamList) {
        this.playerMatchAwayTeamList = playerMatchAwayTeamList;
    }
}
