package com.footballmanagement.dto;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
@MappedSuperclass
public abstract class BaseDTO {
    @Id
    protected Long id;

    public BaseDTO(){

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseDTO)) return false;

        BaseDTO baseDTO = (BaseDTO) o;

        return getId() != null ? getId().equals(baseDTO.getId()) : baseDTO.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}
