package com.footballmanagement.dto;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
public class RoundDTO extends BaseDTO {
    private String name;

    public RoundDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
