package com.footballmanagement.dto;

import com.footballmanagement.model.Team;

/**
 * Created by Chien Nguyen on 03/07/2017.
 */
public class TeamDTO extends BaseDTO {

    private String name;

    public TeamDTO() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
