package com.footballmanagement.dto;


/**
 * Created by Chien Nguyen on 06/07/2017.
 */
public class PlayerMatchDTO extends BaseDTO {

    private PlayerDTO player;
    private MatchDTO match;
    private Integer yellowCard = 0;
    private Integer redCard = 0;
    private Integer goalNumber = 0;
    private Integer ownGoalNumber = 0;

    public PlayerMatchDTO() {
    }

    public PlayerDTO getPlayer() {
        return player;
    }

    public void setPlayer(PlayerDTO player) {
        this.player = player;
    }

    public MatchDTO getMatch() {
        return match;
    }

    public void setMatch(MatchDTO match) {
        this.match = match;
    }

    public Integer getYellowCard() {
        return yellowCard;
    }

    public void setYellowCard(Integer yellowCard) {
        this.yellowCard = yellowCard;
    }

    public Integer getRedCard() {
        return redCard;
    }

    public void setRedCard(Integer redCard) {
        this.redCard = redCard;
    }

    public Integer getGoalNumber() {
        return goalNumber;
    }

    public void setGoalNumber(Integer goalNumber) {
        this.goalNumber = goalNumber;
    }

    public Integer getOwnGoalNumber() {
        return ownGoalNumber;
    }

    public void setOwnGoalNumber(Integer ownGoalNumber) {
        this.ownGoalNumber = ownGoalNumber;
    }
}
