package com.footballmanagement.service;

import com.footballmanagement.dto.PlayerDTO;
import com.footballmanagement.dto.TeamDTO;
import com.footballmanagement.model.Team;

import java.util.List;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
public interface TeamService extends BaseService<Team, TeamDTO> {
    List<TeamDTO> search(String q);
    
    List<PlayerDTO> listPlayer(Long id);

    void addPlayer(Long teamId, Long playerId);
}
