package com.footballmanagement.service;

import com.footballmanagement.dto.RoundDTO;
import com.footballmanagement.model.Round;

/**
 * Created by Chien Nguyen on 05/07/2017.
 */
public interface RoundService extends BaseService<Round, RoundDTO> {

}
