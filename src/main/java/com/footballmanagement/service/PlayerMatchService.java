package com.footballmanagement.service;

import com.footballmanagement.dto.PlayerAchievementDTO;
import com.footballmanagement.dto.PlayerMatchDTO;
import com.footballmanagement.model.PlayerMatch;

import java.util.List;

/**
 * Created by Chien Nguyen on 06/07/2017.
 */
public interface PlayerMatchService extends BaseService<PlayerMatch, PlayerMatchDTO> {
    void deleteByMatchId(Long matchId);

    void deleteByPlayerId(Long playerId);

    PlayerMatchDTO getByPlayerIdAndMatchId(Long playerId, Long matchId);

    void updateListPlayerMatchDTO(List<PlayerMatchDTO> playerMatchDTOList);

    List<PlayerAchievementDTO> getListPlayerAchievenment(Integer limit);
}
