package com.footballmanagement.service;

import com.footballmanagement.dto.PlayerDTO;
import com.footballmanagement.model.Player;

import java.util.List;

/**
 * Created by Chien Nguyen on 03/07/2017.
 */
public interface PlayerService extends BaseService<Player, PlayerDTO> {
    List<PlayerDTO> search(String q);

    List<PlayerDTO> listPlayerNotInAnyTeam();

    void addPlayerToTeam(Long teamId, Long playerId);

    void removePlayerFormTheTeam(Long playerId);

    List<PlayerDTO> getPlayersByTeamId(Long teamId);
}
