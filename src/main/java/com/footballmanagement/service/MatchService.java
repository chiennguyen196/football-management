package com.footballmanagement.service;

import com.footballmanagement.dto.MatchDTO;
import com.footballmanagement.dto.TeamAchievementDTO;
import com.footballmanagement.model.Match;

import java.util.List;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
public interface MatchService extends BaseService<Match, MatchDTO> {
    List<MatchDTO> findUpcomingMatches();

    List<MatchDTO> findMatchesHasTakenPlace();

    List<TeamAchievementDTO> getTeamAchievenmentList(Integer limit);
}
