package com.footballmanagement.service.impl;

import com.footballmanagement.dao.MatchDAO;
import com.footballmanagement.dao.PlayerMatchDAO;
import com.footballmanagement.dto.PlayerAchievementDTO;
import com.footballmanagement.dto.PlayerMatchDTO;
import com.footballmanagement.model.PlayerMatch;
import com.footballmanagement.service.PlayerMatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Chien Nguyen on 06/07/2017.
 */
@Service
@Transactional
public class PlayerMatchServiceImpl extends AbstractService<PlayerMatchDAO, PlayerMatch, PlayerMatchDTO>
        implements PlayerMatchService {

    @Override
    @Autowired
    public void setDao(PlayerMatchDAO playerMatchDAO){
        super.setDao(playerMatchDAO);
    }

    @Override
    public void deleteByMatchId(Long matchId) {
        getDao().deleteByMatchId(matchId);
    }

    @Override
    public void deleteByPlayerId(Long playerId) {
        getDao().deleteByPlayerId(playerId);
    }

    @Override
    public PlayerMatchDTO getByPlayerIdAndMatchId(Long playerId, Long matchId) {
        PlayerMatch entity = getDao().getByPlayerIdAndMatchId(playerId, matchId);
        if(entity == null)
            return null;
        return convertEntityToDTO(entity);
    }

    @Override
    public void updateListPlayerMatchDTO(List<PlayerMatchDTO> playerMatchDTOList) {
        List<PlayerMatch> playerMatchList = convertListDTOToEntity(playerMatchDTOList);
        for(PlayerMatch playerMatch : playerMatchList){
            Integer check = playerMatch.getGoalNumber() + playerMatch.getOwnGoalNumber()
                    + playerMatch.getRedCard() + playerMatch.getYellowCard();
            if(playerMatch.getId() != null || check > 0)
                getDao().saveOrUpdate(playerMatch);

        }
    }

    @Override
    public List<PlayerAchievementDTO> getListPlayerAchievenment(Integer limit) {
        return getDao().getListPlayerAchievenment(limit);
    }
}
