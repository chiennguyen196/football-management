package com.footballmanagement.service.impl;

import com.footballmanagement.dao.MatchDAO;
import com.footballmanagement.dao.PlayerDAO;
import com.footballmanagement.dao.TeamDAO;
import com.footballmanagement.dto.PlayerDTO;
import com.footballmanagement.dto.TeamDTO;
import com.footballmanagement.model.Team;
import com.footballmanagement.service.TeamService;
import com.footballmanagement.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
@Service
@Transactional
public class TeamServiceImpl extends AbstractService<TeamDAO, Team, TeamDTO> implements TeamService {

    @Override
    @Autowired
    public void setDao(TeamDAO teamDAO){
        super.setDao(teamDAO);
    }

    @Override
    public List<TeamDTO> search(String q) {
        return convertListEntityToDTO(getDao().search(q));
    }

    @Override
    public List<PlayerDTO> listPlayer(Long id) {
        List allPlayer = getDao().getAllPlayer(id);
        return ModelMapperUtil.mapList(allPlayer, PlayerDTO.class);
    }

    @Override
    public void addPlayer(Long teamId, Long playerId) {
        getDao().addPlayer(teamId, playerId);
    }

    @Autowired
    private PlayerDAO playerDAO;

    @Autowired
    private MatchDAO matchDAO;

    @Override
    public void delete(Long id){
        playerDAO.setTeamIsNull(id);
        matchDAO.removeMatchesByTeamId(id);
        getDao().deleteById(id);
    }
}
