package com.footballmanagement.service.impl;

import com.footballmanagement.dao.RoundDAO;
import com.footballmanagement.dto.RoundDTO;
import com.footballmanagement.model.Round;
import com.footballmanagement.service.RoundService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Chien Nguyen on 05/07/2017.
 */
@Service
@Transactional
public class RoundServiceImpl extends AbstractService<RoundDAO, Round, RoundDTO> implements RoundService {
}
