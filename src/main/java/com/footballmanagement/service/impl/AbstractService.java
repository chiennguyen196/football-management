package com.footballmanagement.service.impl;

import com.footballmanagement.dao.BaseDAO;
import com.footballmanagement.dto.BaseDTO;
import com.footballmanagement.model.BaseEntity;
import com.footballmanagement.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
@Transactional
public abstract class AbstractService<TDAO extends BaseDAO, TEntity extends BaseEntity, TDTO extends BaseDTO> {

    @Autowired
    private TDAO dao;

    private final Class<TEntity> entityClass;

    private final Class<TDTO> dtoClass;

    protected TDAO getDao() {
        return dao;
    }

    public void setDao(TDAO dao) {
        this.dao = dao;
    }

    public AbstractService(){

        this.entityClass =
                (Class<TEntity>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        this.dtoClass =
                (Class<TDTO>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[2];
    }

    protected TEntity convertDTOToEntity(TDTO dto){
        return ModelMapperUtil.map(dto, entityClass);
    }

    protected TDTO convertEntityToDTO(TEntity entity){
        return ModelMapperUtil.map(entity, dtoClass);
    }

    protected List<TEntity> convertListDTOToEntity(List<TDTO> listDTO){
        List<TEntity> list = new ArrayList<TEntity>();
        for (TDTO dto: listDTO) {
            list.add(convertDTOToEntity(dto));
        }
        return list;
    }
    protected List<TDTO> convertListEntityToDTO(List<TEntity> listEntity){
        List<TDTO> list = new ArrayList<TDTO>();
        for(TEntity entity : listEntity){
            list.add(convertEntityToDTO(entity));
        }
        return list;
    }

    public List<TDTO> findAll(){
        return convertListEntityToDTO(dao.findAll());
    }

    public void insert(TDTO dto){
        dao.insert(convertDTOToEntity(dto));
    }

    public void update(TDTO dto){
        dao.update(convertDTOToEntity(dto));
    }

    public void delete(TDTO dto){
        dao.delete(convertDTOToEntity(dto));
    }

    public void delete(Long id){
        dao.deleteById(id);
    }

    public TDTO findById(Long id){
        return convertEntityToDTO((TEntity) dao.findById(id));
    }

    public Long count(){
        return dao.count();
    }

    public void saveOrUpdate(TDTO dto) {
        dao.saveOrUpdate(convertDTOToEntity((TDTO)dto));
    }
}
