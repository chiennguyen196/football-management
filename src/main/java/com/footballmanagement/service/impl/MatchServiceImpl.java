package com.footballmanagement.service.impl;

import com.footballmanagement.dao.MatchDAO;
import com.footballmanagement.dao.PlayerMatchDAO;
import com.footballmanagement.dto.MatchDTO;
import com.footballmanagement.dto.TeamAchievementDTO;
import com.footballmanagement.model.Match;
import com.footballmanagement.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
@Service
@Transactional
public class MatchServiceImpl extends AbstractService<MatchDAO, Match, MatchDTO> implements MatchService {

    @Override
    @Autowired
    public void setDao(MatchDAO matchDAO){
        super.setDao(matchDAO);
    }

    @Override
    public List<MatchDTO> findUpcomingMatches() {
        return convertListEntityToDTO(getDao().findUpcomingMatches());
    }

    @Override
    public List<MatchDTO> findMatchesHasTakenPlace() {
        return convertListEntityToDTO(getDao().findMatchesHasTakenPlace());
    }

    @Override
    public List<TeamAchievementDTO> getTeamAchievenmentList(Integer limit) {
        return getDao().getTeamAchievenmentList(limit);
    }

    @Autowired
    private PlayerMatchDAO playerMatchDAO;

    @Override
    public void delete(Long id){
        playerMatchDAO.deleteByMatchId(id);
        super.delete(id);
    }
}
