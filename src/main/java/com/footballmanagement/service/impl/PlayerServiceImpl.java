package com.footballmanagement.service.impl;

import com.footballmanagement.dao.PlayerDAO;
import com.footballmanagement.dao.TeamDAO;
import com.footballmanagement.dto.PlayerDTO;
import com.footballmanagement.model.Player;
import com.footballmanagement.model.Team;
import com.footballmanagement.service.PlayerService;
import com.footballmanagement.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created by Chien Nguyen on 03/07/2017.
 */
@Service
@Transactional
public class PlayerServiceImpl extends AbstractService<PlayerDAO, Player, PlayerDTO> implements PlayerService{


    @Override
    @Autowired
    public void setDao(PlayerDAO playerDAO){
        super.setDao(playerDAO);
    }

    @Autowired
    private TeamDAO teamDAO;

    @Override
    public List<PlayerDTO> search(String q) {
        return convertListEntityToDTO(getDao().searchByName(q));
    }

    @Override
    public List<PlayerDTO> listPlayerNotInAnyTeam() {
        return convertListEntityToDTO(getDao().listAllPlayersNotInAnyTeam());
    }

    @Override
    public void addPlayerToTeam(Long teamId, Long playerId) {
        Team team = teamDAO.findById(teamId);
        getDao().addPlayerToTeam(team, playerId);
    }

    @Override
    public void removePlayerFormTheTeam(Long playerId) {
        getDao().removePlayerFromTheTeam(playerId);
    }

    @Override
    public List<PlayerDTO> getPlayersByTeamId(Long teamId) {
        return convertListEntityToDTO(getDao().getPlayersByTeamId(teamId));
    }

}
