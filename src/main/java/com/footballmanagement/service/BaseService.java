package com.footballmanagement.service;

import com.footballmanagement.dto.BaseDTO;
import com.footballmanagement.model.BaseEntity;

import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
public interface BaseService<TEntity extends BaseEntity, TDTO extends BaseDTO> {
    List<TDTO> findAll();

    void insert(TDTO dto);

    void update(TDTO dto);

    void delete(TDTO dto);

    void delete(Long id);

    TDTO findById(Long id);

    void saveOrUpdate(TDTO dto);
}
