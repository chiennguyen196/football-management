package com.footballmanagement.util;

import com.footballmanagement.dto.MatchDTO;
import com.footballmanagement.dto.PlayerDTO;
import com.footballmanagement.dto.RoundDTO;
import com.footballmanagement.dto.TeamDTO;
import com.footballmanagement.model.Match;
import com.footballmanagement.model.Player;
import com.footballmanagement.model.Round;
import com.footballmanagement.model.Team;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MappingContext;

import javax.print.attribute.standard.Destination;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chien Nguyen on 03/07/2017.
 */
public class ModelMapperUtil {
    private static ModelMapper modelMapper;

    static {
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);

        modelMapper.addMappings(new PropertyMap<Team, TeamDTO>() {
            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setName(source.getName());
            }
        });

        modelMapper.addMappings(new PropertyMap<Player, PlayerDTO>() {
            @Override
            protected void configure() {
                map(source.getTeam(), destination.getTeam());
            }
        });



        modelMapper.addMappings(new PropertyMap<Round, RoundDTO>() {
            @Override
            protected void configure() {
                map().setId(source.getId());
                using(converter).map(source).setName(null);
            }

            final Converter<Round, String> converter = new AbstractConverter<Round, String>() {
                @Override
                public String convert(Round source) {
                    if(source == null){
                        return null;
                    }
                    else {
                        return "Round " +
                                source.getIndex().toString() + " " +
                                source.getSeason().getName() + " " +
                                source.getSeason().getLeague().getName();
                    }
                }
            };
        });

        modelMapper.addMappings(new PropertyMap<Match, MatchDTO>() {
            @Override
            protected void configure() {
                map(source.getRound()).setRound(null);
            }
        });


    }

    public static <DestObj extends Object> DestObj map(Object object, Class<DestObj> mapClass){
        return (DestObj) modelMapper.map(object, mapClass);
    }

    public static <DestObj extends Object> List<DestObj> mapList(List<Object> objects, Class<DestObj> mapClass) {
        List<DestObj> list = new ArrayList<DestObj>();
        for (Object object : objects){
            list.add(map(object, mapClass));
        }
        return list;
    }
}
