package com.footballmanagement.controller;

import com.footballmanagement.dao.PlayerDAO;
import com.footballmanagement.dao.RoundDAO;
import com.footballmanagement.dto.RoundDTO;
import com.footballmanagement.model.Player;
import com.footballmanagement.model.Round;
import com.footballmanagement.service.RoundService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
@Controller
@RequestMapping("/test")
public class TestController {
    @Autowired
    private PlayerDAO playerDAO;

    @Autowired
    private RoundService roundService;

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    private RoundDAO roundDAO;

    @GetMapping("/")
    public String test(Model model){
//        Round round = roundDAO.findById(new Long(1));
        RoundDTO round = roundService.findById(new Long(1));
        System.out.println("++++++++++++++++++");
        System.out.println(round.getId());
        System.out.println(round.getName());
        System.out.println("++++++++++++++++++");
        model.addAttribute("round", round);
        return "test";
    }
}
