package com.footballmanagement.controller;

import com.footballmanagement.dto.PlayerAchievementDTO;
import com.footballmanagement.service.MatchService;
import com.footballmanagement.service.PlayerMatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by Chien Nguyen on 07/07/2017.
 */
@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private MatchService matchService;

    @Autowired
    private PlayerMatchService playerMatchService;

    @GetMapping("")
    public String home(Model model){
        model.addAttribute("title", "Trang chủ");
        model.addAttribute("upcomingMatches", matchService.findUpcomingMatches());
        model.addAttribute("takenPlaceMatches", matchService.findMatchesHasTakenPlace());
        model.addAttribute("listPlayerAchievenment", playerMatchService.getListPlayerAchievenment(5));
//        List<PlayerAchievementDTO> test = playerMatchService.getListPlayerAchievenment(5);
//        for(PlayerAchievementDTO a : test){
//            System.out.println(a.getName());
//        }

        model.addAttribute("teamAchievenmentList", matchService.getTeamAchievenmentList(5));
        return "home";
    }
}
