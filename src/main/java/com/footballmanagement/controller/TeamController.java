package com.footballmanagement.controller;

import com.footballmanagement.dto.PlayerDTO;
import com.footballmanagement.dto.TeamDTO;
import com.footballmanagement.model.Team;
import com.footballmanagement.service.PlayerService;
import com.footballmanagement.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
@Controller
@RequestMapping("/team")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private PlayerService playerService;

    @GetMapping(path = {"/", "/list", ""})
    public String list(Model model){
        List<TeamDTO> teams = teamService.findAll();
        model.addAttribute("title", "Danh sach team");
        model.addAttribute("teams", teams);
        return "team/list";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable Long id, Model model){
        TeamDTO teamDTO = teamService.findById(id);
        model.addAttribute("title", "Chinh sua team");
        model.addAttribute("teamDTO", teamDTO);
        return "team/form";
    }

    @GetMapping("/create")
    public String create(Model model){
        TeamDTO teamDTO = new TeamDTO();
        model.addAttribute("title", "Them moi team");
        model.addAttribute("teamDTO", teamDTO);
        return "team/form";
    }

    @PostMapping("/save")
    public String save(@Valid TeamDTO teamDTO, BindingResult result, RedirectAttributes redirect){
        if(result.hasErrors()){
            return "team/form";
        }
        teamService.saveOrUpdate(teamDTO);

        redirect.addFlashAttribute("message", "Save thanh cong!");
        return "redirect:/team/list";
    }

//    Quan ly thanh vien cua doi bong

    @GetMapping("/{id}/member")
    public String listMember(@PathVariable Long id, Model model){
        List<PlayerDTO> members = teamService.listPlayer(id);
        model.addAttribute("team", teamService.findById(id));
        model.addAttribute("title", "Danh sach thanh vien");
        model.addAttribute("players", members);
        return "team/list-member";
    }

    @GetMapping("/{teamId}/add-player")
    public String listMemberToAdd(@PathVariable Long teamId, Model model){
        TeamDTO teamDTO = teamService.findById(teamId);
        List<PlayerDTO> players = playerService.listPlayerNotInAnyTeam();
        model.addAttribute("title", "Them thanh vien vao doi");
        model.addAttribute("team", teamDTO);
        model.addAttribute("players", players);
        return "team/add-member";
    }

    @GetMapping("/{teamId}/add-player/{playerId}")
    public String addMember(@PathVariable Long teamId, @PathVariable Long playerId, RedirectAttributes redirect){
        playerService.addPlayerToTeam(teamId, playerId);
        redirect.addFlashAttribute("message", "Them thanh cong!");
        return "redirect:/team/" + teamId + "/member";
    }

    @GetMapping("/{teamId}/remove-player/{playerId}")
    public String removeMember(@PathVariable Long teamId, @PathVariable Long playerId, RedirectAttributes redirect){
        playerService.removePlayerFormTheTeam(playerId);
        redirect.addFlashAttribute("message", "Xoa thanh cong!");
        return "redirect:/team/" + teamId + "/member";
    }

    @GetMapping("/{teamId}/delete")
    public String deleteTeam(@PathVariable Long teamId, RedirectAttributes redirect){
        teamService.delete(teamId);
        redirect.addFlashAttribute("message", "Xoa thanh cong!");
        return "redirect:/team/list";
    }
}
