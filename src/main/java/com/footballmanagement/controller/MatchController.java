package com.footballmanagement.controller;

import com.footballmanagement.dto.*;
import com.footballmanagement.model.PlayerMatch;
import com.footballmanagement.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Chien Nguyen on 04/07/2017.
 */
@Controller
@RequestMapping("/match")
public class MatchController {

    @Autowired
    private MatchService matchService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private RoundService roundService;

    @RequestMapping({"", "/", "/list"})
    public String list(Model model) {
        List<MatchDTO> matches = matchService.findAll();
        model.addAttribute("title", "Danh sach tran dau");
        model.addAttribute("matches", matches);
        return "match/list";
    }

    @RequestMapping("/upcoming")
    public String listUpcomingMatches(Model model) {
        List<MatchDTO> matches = matchService.findUpcomingMatches();
        model.addAttribute("title", "Danh sach tran dau sap dien ra");
        model.addAttribute("matches", matches);
        return "match/list";
    }

    @RequestMapping("/has-taken-place")
    public String listMatchesHasTakenPlace(Model model) {
        List<MatchDTO> matches = matchService.findMatchesHasTakenPlace();
        model.addAttribute("title", "Danh sach tran da dien ra");
        model.addAttribute("matches", matches);
        return "match/list";
    }

    @RequestMapping("/add-new")
    public String addNew(Model model) {
        MatchDTO matchDTO = new MatchDTO();
        Map<String, String> teams = new LinkedHashMap<>();
        for (TeamDTO teamDTO : teamService.findAll()) {
            teams.put(teamDTO.getId().toString(), teamDTO.getName());
        }

        Map<String, String> rounds = new LinkedHashMap<>();
        for (RoundDTO roundDTO : roundService.findAll()) {
            rounds.put(roundDTO.getId().toString(), roundDTO.getName());
        }
        model.addAttribute("title", "Them moi tran dau");
        model.addAttribute("matchDTO", matchDTO);
        model.addAttribute("teams", teams);
        model.addAttribute("rounds", rounds);
        return "match/add-new";
    }

    @PostMapping("/add-new")
    public String saveAddNew(@Valid MatchDTO matchDTO, BindingResult result, RedirectAttributes redirect) {
        if (result.hasErrors()) {
            redirect.addFlashAttribute("Them that bai!");
            return "redirect:/match";
        }
        matchService.saveOrUpdate(matchDTO);
        redirect.addFlashAttribute("Them thanh cong!");
        return "redirect:/match";
    }

    @RequestMapping("/{id}/delete")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        matchService.delete(id);
        redirect.addFlashAttribute("Xoa thanh cong!");
        return "redirect:/match";
    }

    @Autowired
    private PlayerService playerService;

    @RequestMapping("/{id}/edit")
    public String edit(@PathVariable Long id, Model model) {
        MatchDTO matchDTO = matchService.findById(id);

        List<PlayerDTO> homeTeamPlayers = playerService.getPlayersByTeamId(matchDTO.getHomeTeam().getId());
        List<PlayerMatchDTO> playerMatchHomeTeamList = createPlayerMatchDTOList(matchDTO, homeTeamPlayers);


        List<PlayerDTO> awayTeamPlayers = playerService.getPlayersByTeamId(matchDTO.getAwayTeam().getId());
        List<PlayerMatchDTO> playerMatchAwayTeamList = createPlayerMatchDTOList(matchDTO, awayTeamPlayers);


        PlayerMatchDTOWrapper playerMatchDTOWrapper = new PlayerMatchDTOWrapper();
        playerMatchDTOWrapper.setPlayerMatchHomeTeamList(playerMatchHomeTeamList);
        playerMatchDTOWrapper.setPlayerMatchAwayTeamList(playerMatchAwayTeamList);
        playerMatchDTOWrapper.setMatch(matchDTO);

        model.addAttribute("match", matchDTO);
        model.addAttribute("playerMatchHomeTeamList", playerMatchHomeTeamList);
        model.addAttribute("playerMatchAwayTeamList", playerMatchAwayTeamList);
        model.addAttribute("title", "Chỉnh sửa trận đấu");
        model.addAttribute("playerMatchDTOWrapper", playerMatchDTOWrapper);
        return "match/edit";
    }

    @Autowired
    private PlayerMatchService playerMatchService;

    private List<PlayerMatchDTO> createPlayerMatchDTOList(MatchDTO matchDTO, List<PlayerDTO> playerDTOList) {
        List<PlayerMatchDTO> playerMatchList = new LinkedList<>();
        for (PlayerDTO playerDTO : playerDTOList) {
            PlayerMatchDTO playerMatchDTO =
                    playerMatchService.getByPlayerIdAndMatchId(playerDTO.getId(), matchDTO.getId());
            if(playerMatchDTO == null){
                playerMatchDTO = new PlayerMatchDTO();
                playerMatchDTO.setMatch(matchDTO);
                playerMatchDTO.setPlayer(playerDTO);
            }
            playerMatchList.add(playerMatchDTO);
        }
        return playerMatchList;
    }



    @PostMapping("/edit")
    public String updateMatch(@Valid PlayerMatchDTOWrapper playerMatchDTOWrapper
            , BindingResult result, RedirectAttributes redirect){
        if(result.hasErrors()){
            redirect.addFlashAttribute("Update that bai!");
            return "redirect:/match";
        }

        MatchDTO matchDTO = playerMatchDTOWrapper.getMatch();
        System.out.println();
        matchService.saveOrUpdate(matchDTO);

        List<PlayerMatchDTO> playerMatchDTOList = new LinkedList<>();
        playerMatchDTOList.addAll(playerMatchDTOWrapper.getPlayerMatchHomeTeamList());
        playerMatchDTOList.addAll(playerMatchDTOWrapper.getPlayerMatchAwayTeamList());

        playerMatchService.updateListPlayerMatchDTO(playerMatchDTOList);

        return "redirect:/match";
    }

}
