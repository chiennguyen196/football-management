package com.footballmanagement.controller;

import com.footballmanagement.dto.PlayerDTO;
import com.footballmanagement.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
@Controller
@RequestMapping("/player")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @GetMapping(path = {"/", "/list", ""})
    public String list(Model model){
        List<PlayerDTO> players = playerService.findAll();
        model.addAttribute("title", "Danh sách cầu thủ.");
        model.addAttribute("players", players);
        return "player/list";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        PlayerDTO playerDTO = playerService.findById(id);

        model.addAttribute("title", "Chỉnh sửa player");
        model.addAttribute("playerDTO", playerDTO);
        return "player/form";
    }

    @GetMapping("/create")
    public String create(Model model){
        PlayerDTO playerDTO = new PlayerDTO();

        model.addAttribute("title", "Thêm mới player");
        model.addAttribute("playerDTO", playerDTO);
        return "player/form";
    }

    @PostMapping("/save")
    public String save(@Valid PlayerDTO playerDTO, BindingResult result, RedirectAttributes redirect){
        if(result.hasErrors()){
            System.out.println("Error");

            for (ObjectError error : result.getAllErrors()){
                System.out.println(error);
            }
            return "player/form";
        }
        playerService.saveOrUpdate(playerDTO);
        redirect.addFlashAttribute("message", "Save thanh cong!");
        return "redirect:/player/list";
    }
    
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id, RedirectAttributes redirect){
        playerService.delete(id);
        redirect.addFlashAttribute("message", "Xoa thanh cong!");
        return "redirect:/player/list";
    }

}
