package com.footballmanagement.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Chien Nguyen on 29/06/2017.
 */

@Entity
@Table(name = "leagues")
public class League extends BaseEntity {
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "league")
    private List<Season> seasons;

    public League() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
