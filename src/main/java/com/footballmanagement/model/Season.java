package com.footballmanagement.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Chien Nguyen on 29/06/2017.
 */
@Entity
@Table(name = "seasons")
public class Season extends BaseEntity {
    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "league_id")
    private League league;

    @OneToMany(mappedBy = "season", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Round> rounds;

    public Season() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }
}
