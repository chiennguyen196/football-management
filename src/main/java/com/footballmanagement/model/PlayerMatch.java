package com.footballmanagement.model;

import javax.persistence.*;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
@Entity
@Table(name = "player_match")
public class PlayerMatch extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "match_id")
    private Match match;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;

    @Column(name = "yellow_card")
    private Integer yellowCard;

    @Column(name = "red_card")
    private Integer redCard;

    @Column(name = "goal_number")
    private Integer goalNumber;

    @Column(name = "own_goal_number")
    private Integer ownGoalNumber;

    public PlayerMatch() {
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Integer getYellowCard() {
        return yellowCard;
    }

    public void setYellowCard(Integer yellowCard) {
        this.yellowCard = yellowCard;
    }

    public Integer getRedCard() {
        return redCard;
    }

    public void setRedCard(Integer redCard) {
        this.redCard = redCard;
    }

    public Integer getGoalNumber() {
        return goalNumber;
    }

    public void setGoalNumber(Integer goalNumber) {
        this.goalNumber = goalNumber;
    }

    public Integer getOwnGoalNumber() {
        return ownGoalNumber;
    }

    public void setOwnGoalNumber(Integer ownGoalNumber) {
        this.ownGoalNumber = ownGoalNumber;
    }
}
