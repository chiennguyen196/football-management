package com.footballmanagement.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Chien Nguyen on 30/06/2017.
 */
@Entity
@Table(name = "rounds")
public class Round extends BaseEntity {
    @Column(name = "index")
    private Integer index;

    @ManyToOne
    @JoinColumn(name = "season_id")
    private Season season;

    @OneToMany(mappedBy = "round", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Match> matchList;

    public Round() {
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }
}
